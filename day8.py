from common import get_input

inputs = [int(x) for x in get_input(__file__).split()]


class Node:
    def __init__(self, content):
        nb_childs = content.pop(0)
        nb_metadata = content.pop(0)
        self.children = []
        self.metadata = []
        for _ in range(nb_childs):
            self.children.append(Node(content))
        for _ in range(nb_metadata):
            self.metadata.append(content.pop(0))

    def sum(self) -> int:
        total = sum(self.metadata)
        for child in self.children:
            total += child.sum()
        return total

    def value(self) -> int:
        if not self.children:
            return self.sum()
        total = 0
        for child_index in self.metadata:
            if 0 < child_index <= len(self.children):
                total += self.children[child_index - 1].value()
        return total


def part1():
    content = list(inputs)
    root = Node(content)
    assert not content
    assert inputs

    return root.sum()


def part2():
    content = list(inputs)
    root = Node(content)
    assert not content
    assert inputs

    return root.value()


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
