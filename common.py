from pathlib import Path


def get_input(f: str) -> str:
    p = (Path("inputs") / f).with_suffix(".txt")
    return p.read_text()


def get_inputs(f: str) -> list:
    return get_input(f).splitlines()
