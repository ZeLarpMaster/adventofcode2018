import collections

from common import get_inputs

inputs = get_inputs(__file__)


def parse_sleepiness():
    current_guard = None
    slept_at = None
    sleep_by_guard = collections.Counter()
    time_asleep_by_guard = {}

    for log in sorted(inputs):
        dt, rest = log[6:].split("] ")
        day, time = dt.split(" ")
        h, m = time.split(":")

        if rest.endswith("begins shift"):
            if len(time_asleep_by_guard.get(current_guard, [0])) == 0:
                del time_asleep_by_guard[current_guard]
            current_guard = rest[len("Guard #"):-len(" begins shift")]
            time_asleep_by_guard.setdefault(current_guard, collections.Counter())
        elif rest.endswith("falls asleep"):
            slept_at = int(m)
        elif rest.endswith("wakes up"):
            sleep_by_guard[current_guard] += int(m) - slept_at
            time_asleep_by_guard[current_guard].update(range(slept_at, int(m)))

    return sleep_by_guard, time_asleep_by_guard


def part1():
    sleep_by_guard, time_asleep_by_guard = parse_sleepiness()
    sleepiest = sleep_by_guard.most_common()[0][0]
    most_sleepy_at = time_asleep_by_guard[sleepiest].most_common()[0][0]
    return int(sleepiest) * most_sleepy_at


def part2():
    _, time_asleep_by_guard = parse_sleepiness()
    sleepiest = None
    most_sleepiness = None
    most_sleepy_time = None
    for guard_id, times_asleep in time_asleep_by_guard.items():
        most_asleep_at, sleepiness = times_asleep.most_common(1)[0]
        if most_sleepiness is None or sleepiness > most_sleepiness:
            sleepiest = guard_id
            most_sleepiness = sleepiness
            most_sleepy_time = most_asleep_at
    return int(sleepiest) * most_sleepy_time


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
