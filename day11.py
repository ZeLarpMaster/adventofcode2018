import collections
import functools


SERIAL = 5468
TABLE = []


@functools.lru_cache(300 * 300)
def get_power(x, y):
    power = x + 10
    power *= (y * power) + SERIAL
    hundreds = (power // 100) % 10
    return hundreds - 5


def generate_table():
    prev_row = None
    for y in range(300):
        row = []
        for x in range(300):
            value = get_power(x + 1, y + 1)
            if row:
                value += row[-1]
            if prev_row:
                value += prev_row[x]
                if x > 0:
                    value -= prev_row[x - 1]
            row.append(value)
        TABLE.append(row)
        prev_row = row


def calc(min_x, min_y, max_x, max_y) -> int:
    a = TABLE[min_y][min_x] if min_x >= 0 and min_y >= 0 else 0
    b = TABLE[min_y][max_x] if min_y >= 0 else 0
    c = TABLE[max_y][min_x] if min_x >= 0 else 0
    d = TABLE[max_y][max_x]
    return a - b - c + d


def part1():
    max_coords = None
    max_value = None
    for x in range(298):
        for y in range(298):
            value = calc(x - 1, y - 1, x + 2, y + 2)
            if max_value is None or value > max_value:
                max_value = value
                max_coords = x + 1, y + 1
    return ",".join(map(str, max_coords))


def part2():
    max_coords = None
    max_value = None
    for x in range(298):
        for y in range(298):
            for size in range(300 - max(x, y)):
                value = calc(x - 1, y - 1, x + size, y + size)
                if max_value is None or value > max_value:
                    max_value = value
                    max_coords = x + 1, y + 1, size + 1
    return ",".join(map(str, max_coords))


if __name__ == "__main__":
    generate_table()
    print("Part 1 =", part1())
    print("Part 2 =", part2())
