import collections

from common import get_inputs

coords = [tuple(map(int, line.split(", "))) for line in get_inputs(__file__)]
furthest_right = max(c[0] for c in coords) + 1
furthest_down = max(c[1] for c in coords) + 1
furthest_left = min(c[0] for c in coords) - 1
furthest_up = min(c[1] for c in coords) - 1


def mandist(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1])


def part1():
    infinite = set()
    area = collections.Counter()
    for x in range(furthest_left, furthest_right + 1):
        for y in range(furthest_up, furthest_down + 1):
            pos = x, y
            min_dist = min(mandist(pos, coord) for coord in coords)
            closests = [coord for coord in coords if mandist(pos, coord) == min_dist]
            if len(closests) == 1:
                area[closests[0]] += 1
                if x in (furthest_left, furthest_right) or y in (furthest_down, furthest_up):
                    infinite.add(closests[0])

    for infinite_pos in infinite:
        del area[infinite_pos]

    return area.most_common(1)[0][1]


def part2():
    region_size = 0
    for x in range(furthest_left + 1, furthest_right - 1):
        for y in range(furthest_up + 1, furthest_down - 1):
            pos = x, y
            dist = sum(mandist(pos, coord) for coord in coords)
            if dist < 10000:
                region_size += 1
    return region_size


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
