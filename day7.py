import string

from common import get_inputs

inputs = get_inputs(__file__)


def parse_dependencies():  # (A, B) such that A must be done before B
    waiting = set()
    blocked_by = {}
    for a, b in ((line[5], line[36]) for line in inputs):
        blocked_by.setdefault(b, set()).add(a)
        waiting.add(a)
        waiting.add(b)
    return waiting, blocked_by


def part1():
    waiting, blocked_by = parse_dependencies()
    path = ""
    while waiting:
        can_be_done = sorted(x for x in waiting if x not in blocked_by or not blocked_by[x])
        doing = can_be_done.pop(0)
        waiting.remove(doing)
        for a, b in blocked_by.items():
            b.discard(doing)
        path += doing
    return path


def part2():
    cost = {letter: i + 61 for i, letter in enumerate(string.ascii_uppercase)}
    waiting, blocked_by = parse_dependencies()
    time_left = {step: cost[step] for step in waiting}
    time = 0
    while waiting:
        can_be_done = sorted(x for x in waiting if x not in blocked_by or not blocked_by[x])
        for i in range(5):  # 5 workers
            if can_be_done:
                doing = can_be_done.pop(0)
                time_left[doing] -= 1
                if time_left[doing] == 0:
                    for a, b in blocked_by.items():
                        b.discard(doing)
                    waiting.remove(doing)
        time += 1
    return time


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
