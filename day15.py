from collections import defaultdict
from queue import PriorityQueue

from common import get_inputs


inputs = get_inputs(__file__)
terrain = defaultdict(lambda: '.')
goblins = {}
elves = {}
HP, ATTACK = 200, 3
AROUND = (-1, 0), (0, -1), (0, 1), (1, 0)


def parse_input():
    for y, row in enumerate(inputs):
        for x, val in enumerate(row):
            if val != '.':
                terrain[(y, x)] = val
            if val == 'G':
                goblins[(y, x)] = HP
            elif val == 'E':
                elves[(y, x)] = HP


def display_terrain():
    height, width = len(inputs), len(inputs[0])
    for y in range(height):
        hps = []
        for x in range(width):
            pos = y, x
            if pos in goblins:
                print("G", end="")
                hps.append(f"G({goblins[pos]})")
            elif pos in elves:
                print("E", end="")
                hps.append(f"E({elves[pos]})")
            else:
                print(terrain[pos], end="")
        if hps:
            print("  ", ", ".join(hps), end="")
        print()


def get_neighbor_target(pos, enemies):
    y, x = pos
    for dy, dx in AROUND:
        new_pos = y + dy, x + dx
        if new_pos in enemies:
            return new_pos


def find_lowest_hp(pos, enemies):
    y, x = pos
    hps = []
    for dy, dx in AROUND:
        npos = y + dy, x + dx
        if npos in enemies:
            hps.append((enemies[npos], npos))
    return min(hps, key=lambda o: o[1], default=None)


def a_star(start, enemies, allies):
    q = PriorityQueue()
    q.put((0, start, set(), None))
    print("A* from", start, "to", enemies)
    while not q.empty():
        _, pos, path, last_move = q.get()
        print("Checking pos", pos)
        if pos in enemies:
            return last_move
        y, x = pos
        for dy, dx in AROUND:
            npos = y + dy, x + dx
            if npos not in allies and npos not in path and terrain.get(npos, ".") == ".":
                q.put((len(path) + 1, npos, path | {npos}, (dy, dx)))


def part1():
    full_turns = 0
    while True:
        to_sim = sorted(list(goblins.items()) + list(elves.items()), key=lambda o: o[0][0] * len(inputs[0]) + o[0][1])
        for pos, hp in to_sim:
            is_elf = pos in elves
            print("Checking at", pos, "with hp", hp, "E" if is_elf else "G")
            allies = elves if is_elf else goblins
            enemies = goblins if is_elf else elves
            if len(enemies) == 0:
                print(allies)
                return full_turns * (sum(allies.values()) + sum(enemies.values()))
            target = get_neighbor_target(pos, enemies)
            print("Target:", target)
            if target is None:
                move = a_star(pos, enemies, allies)
                print("Move:", move)
                if move is not None:
                    my_hp = allies.pop(pos)
                    y, x = pos
                    dy, dx = move
                    pos = y + dy, x + dx
                    allies[pos] = my_hp
                    target = get_neighbor_target(pos, enemies)
            if target is not None:
                y, x = pos
                target = find_lowest_hp(pos, enemies)
                assert target is not None, f"Failed to pick a target out of {target} and min of {min_hp}"
                hp, target = target
                enemies[target] -= ATTACK
                if enemies[target] <= 0:
                    del enemies[target]
            print("=" * 5, "After", full_turns, "turns", "=" * 5)
            display_terrain()
            exit(1)
        full_turns += 1
        print("=" * 5, "After", full_turns, "turns", "=" * 5)
        display_terrain()
        exit(1)


def part2():
    pass


if __name__ == "__main__":
    parse_input()
    print("Part 1 =", part1())
    print("Part 2 =", part2())
