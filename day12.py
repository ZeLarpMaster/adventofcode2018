import collections

from common import get_inputs

inputs = get_inputs(__file__)
INITIAL_STATE = inputs[0].lstrip("initial state: ").strip()
INITIAL_SIZE = len(INITIAL_STATE)
RULES = {rule[:5] for rule in inputs[2:] if rule[-1] == "#"}


def simulate(state):
    min_i = min(state)
    max_i = max(state)
    new_state = collections.defaultdict(lambda: ".")
    for i in range(min_i - 2, max_i + 3):
        seq = "".join(state[i + d] for d in range(-2, 3))
        if seq in RULES:  # Will exist
            new_state[i] = "#"
    return new_state


def eval_state(state):
    return sum(i for i, x in state.items() if x == "#")


def part1():
    state = collections.defaultdict(lambda: ".", enumerate(INITIAL_STATE))
    for _ in range(20):
        state = simulate(state)
    return eval_state(state)


def part2():
    state = collections.defaultdict(lambda: ".", enumerate(INITIAL_STATE))
    nb = 50000000000
    diffs = collections.deque(maxlen=10)
    prev_score = eval_state(state)
    for i in range(nb):
        state = simulate(state)
        score = eval_state(state)
        diffs.append(score - prev_score)
        print(f"\rEvaluated {i} steps with a diff of: {list(diffs)}{' ' * 20}", end="")
        prev_score = score
        if len(set(diffs)) == 1 and len(diffs) > 1:
            break
    print()
    return prev_score + diffs[0] * (nb - i - 1)


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
