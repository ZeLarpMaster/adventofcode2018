import collections

from common import get_inputs

inputs = get_inputs(__file__)


def part1():
    solution = collections.Counter()
    for i in inputs:
        c = collections.Counter(i)
        c = {x for _, x in c.most_common() if x > 1}
        solution.update(c)
    return solution[2] * solution[3]


def part2():
    for i in range(len(inputs)):
        a = inputs[i]
        for j in range(i, len(inputs)):
            b = inputs[j]
            diffs = []
            for k in range(len(a)):
                if a[k] != b[k]:
                    diffs.append(k)
            if len(diffs) == 1:
                k = diffs[0]
                return a[:k] + a[k + 1:]


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
