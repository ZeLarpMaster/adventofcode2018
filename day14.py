import collections

from common import get_inputs

inputs = get_inputs(__file__)
num_recipes = int(inputs[0])


def part1():
    scores = [3, 7]
    elf1 = 0
    elf2 = 1
    while len(scores) < num_recipes + 10:
        new = scores[elf1] + scores[elf2]
        scores.extend(map(int, str(new)))
        elf1 += scores[elf1] + 1
        elf2 += scores[elf2] + 1
        elf1 %= len(scores)
        elf2 %= len(scores)
    return "".join(map(str, scores[num_recipes:num_recipes + 10]))


def part2():
    num_recipes = 147061
    expected_recipe = str(num_recipes)
    last_few_scores = collections.deque(["3", "7"], maxlen=len(expected_recipe))
    scores = [3, 7]
    elf1 = 0
    elf2 = 1
    while True:
        new = scores[elf1] + scores[elf2]
        new_scores = list(map(int, str(new)))
        scores.extend(new_scores)
        elf1 += scores[elf1] + 1
        elf2 += scores[elf2] + 1
        elf1 %= len(scores)
        elf2 %= len(scores)
        for score in new_scores:
            last_few_scores.append(str(score))
            if expected_recipe == "".join(last_few_scores):
                return len(scores) - len(last_few_scores) - 1


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
