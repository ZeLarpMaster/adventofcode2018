import collections

from common import get_inputs

inputs = get_inputs(__file__)


def parse_claim(line: str) -> tuple:
    #  Example: "#1 @ 1,3: 4x4"
    claim_id, rest = line[1:].split(" @ ")
    offsets, sizes = rest.split(": ")
    left, top = offsets.split(",")
    width, height = sizes.split("x")
    return claim_id, int(left), int(top), int(width), int(height)


def part1():
    claims = collections.Counter()
    for line in inputs:
        _, left, top, width, height = parse_claim(line)
        for i in range(width):
            for j in range(height):
                claims[(left + i, top + j)] += 1
    collisions = {pos for pos, cnt in claims.items() if cnt > 1}
    return len(collisions)


def part2():
    ids = set()
    seen = dict()
    for line in inputs:
        claim_id, left, top, width, height = parse_claim(line)
        ids.add(claim_id)
        for i in range(width):
            for j in range(height):
                pos = left + i, top + j
                if pos not in seen:
                    seen[pos] = claim_id
                else:
                    ids.discard(claim_id)
                    ids.discard(seen[pos])
    return ids


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
