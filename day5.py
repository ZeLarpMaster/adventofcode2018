from common import get_input

polymer = get_input(__file__).strip()


def remove(polymer, position):
    return polymer[:max(0, position - 1)] + polymer[min(position + 1, len(polymer)):]


def react(result):
    i = 0
    expectation = None
    while i < len(result):
        c = result[i]
        if c == expectation:
            result = remove(result, i)
            expectation = None
            i -= 3
        else:
            expectation = c.swapcase()
        i += 1
        i = max(0, i)

    return result


def part1():
    return len(react(polymer))


def part2():
    letters = {l.lower() for l in polymer}
    return min(len(react(polymer.replace(l, "").replace(l.upper(), ""))) for l in letters)


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
