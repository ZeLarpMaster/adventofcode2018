import collections
import itertools

PLAYERS = 405
MARBLES = 71700


def part1():
    player = itertools.cycle(range(1, PLAYERS + 1))
    scores = collections.Counter()
    marbles = DLList(0)
    current_marble = marbles.head
    expect = 1000
    for i in range(1, MARBLES + 1):
        current_player = next(player)
        if i % 23 == 0:  # Special case
            current_marble = current_marble.go_left(9)
            removed = marbles.pop(after=current_marble)
            scores[current_player] += removed + i
        else:  # Insert marble
            marbles.insert(i, after=current_marble)
        current_marble = current_marble.go_right(2)
    return scores.most_common(1)[0][1]


def part2():
    global MARBLES
    MARBLES *= 100
    return part1()


class DLNode:
    """A node in a doubly-linked list"""

    __slots__ = "data", "prev", "next"

    def __init__(self, data: int=None, prev: 'DLNode'=None, nxt: 'DLNode'=None):
        self.data = data
        self.prev = prev
        self.next = nxt

    def go_left(self, n: int) -> 'DLNode':
        cur = self
        for _ in range(n):
            cur = cur.prev
        return cur

    def go_right(self, n: int) -> 'DLNode':
        cur = self
        for _ in range(n):
            cur = cur.next
        return cur

    def __repr__(self) -> str:
        return repr(self.data)


class DLList:

    __slots__ = ("head", )

    def __init__(self, value: int):
        self.head = DLNode(value)
        self.head.prev = self.head
        self.head.next = self.head

    def __repr__(self) -> str:
        cur = self.head
        seen = []
        while cur is not self.head or not seen:
            seen.append(cur.data)
            cur = cur.next
        return " ".join(f"{x}" for x in reversed(seen))

    def _insert(self, value: int, node: DLNode):
        """Inserts between node.prev and node"""
        new = DLNode(value, node.prev, node)
        node.prev.next = new
        node.prev = new

    def insert(self, value: int, *, before: DLNode=None, after: DLNode=None):
        assert (before is None) != (after is None), "Must specify either before or after"
        if before is not None:
            self._insert(value, before)
        else:
            self._insert(value, after.next)

    def _pop(self, node: DLNode) -> int:
        """Pops the node and returns its value"""
        node.prev.next = node.next
        node.next.prev = node.prev
        return node.data

    def pop(self, *, before: DLNode=None, after: DLNode=None) -> int:
        assert (before is None) != (after is None), "Must specify either before or after"
        if before is not None:
            return self._pop(before)
        else:
            return self._pop(after.next)


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
