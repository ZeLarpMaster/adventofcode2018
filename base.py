from common import get_inputs

inputs = get_inputs(__file__)


def part1():
    pass


def part2():
    pass


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
