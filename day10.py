import math

from common import get_inputs

inputs = get_inputs(__file__)


class Dot:
    def __init__(self, x, y, dx, dy):
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy

    def forward(self):
        self.x += self.dx
        self.y += self.dy

    def backward(self):
        self.x -= self.dx
        self.y -= self.dy

    def dist(self, other) -> int:
        return abs(self.x - other.x) + abs(self.y - other.y)

    def __repr__(self) -> str:
        return f"({self.x}, {self.y})"


def parse_line(line):
    # position=<-50948,  20587> velocity=< 5, -2>
    line = line.strip("position=<>")
    pos_part, vel_part = line.split("> velocity=<")
    x, y = pos_part.split(", ")
    dx, dy = vel_part.split(", ")
    return Dot(int(x), int(y), int(dx), int(dy))


def get_boundaries(dots):
    min_x = math.inf
    min_y = math.inf
    max_x = -math.inf
    max_y = -math.inf
    for dot in dots:
        if dot.x < min_x:
            min_x = dot.x
        if dot.y < min_y:
            min_y = dot.y
        if dot.x > max_x:
            max_x = dot.x
        if dot.y > max_y:
            max_y = dot.y
    return min_x, max_x, min_y, max_y


def get_total_dist(dots):
    min_x, max_x, min_y, max_y = get_boundaries(dots)
    return abs(max_x - min_x) + abs(max_y - min_y)


def simulate():
    dots = [parse_line(line) for line in inputs]
    prev = get_total_dist(dots)
    iterations = 0
    while True:
        for dot in dots:
            dot.forward()
        iterations += 1
        new = get_total_dist(dots)
        if new > prev:
            break
        prev = new
    for dot in dots:
        dot.backward()
    iterations -= 1
    min_x, max_x, min_y, max_y = get_boundaries(dots)
    width = max_x - min_x
    height = max_y - min_y
    output = [list([" "] * (width + 1)) for _ in range(height + 1)]
    for dot in dots:
        output[dot.y - min_y][dot.x - min_x] = "X"
    return iterations, "\n".join("".join(line) for line in output)


def part1():
    return "\n" + simulate()[1]


def part2():
    return simulate()[0]


if __name__ == "__main__":
    print("Part 1 =", part1())
    print("Part 2 =", part2())
