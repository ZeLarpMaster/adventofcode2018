import collections

from common import get_inputs

inputs = get_inputs(__file__)
rails = {}
carts = {}
DIRECTIONS = {"^": (-1, 0), "v": (1, 0), "<": (0, -1), ">": (0, 1), "X": (0, 0)}
ROTATIONS = {"^": ["<", "^", ">"], "v": [">", "v", "<"], "<": ["v", "<", "^"], ">": ["^", ">", "v"]}
TURNS = {"/": {"^": ">", ">": "^", "<": "v", "v": "<"}, "\\": {"^": "<", "<": "^", ">": "v", "v": ">"}}


class Cart:
    cart_id = 0

    def __init__(self, y, x, direction):
        self.y = y
        self.x = x
        self.dir = direction
        self.collided_with = None
        self.next_rotation = 0
        self.id = Cart.cart_id
        Cart.cart_id += 1

    def __str__(self):
        return f"<Cart {self.id} {self.dir} ({self.y}, {self.x})>"

    def __eq__(self, other):
        return self.id == other.id

    def move(self):
        if self.dir == "X":
            return

        dy, dx = DIRECTIONS[self.dir]
        self.x += dx
        self.y += dy
        self.collided_with = carts.pop((self.y, self.x), self.collided_with)
        if self.collided_with is not None:
            assert self.collided_with != self, "Collided with self"
            print(f"Cart {self} collided with {self.collided_with}")
            self.dir = "X"
            self.collided_with.dir = "X"
            self.collided_with.collided_with = self
            return

        current_rail = rails[(self.y, self.x)]

        # Taking a turn on corners
        self.dir = TURNS.get(current_rail, {}).get(self.dir, self.dir)

        # Taking a turn on intersections
        if current_rail == "+":
            self.dir = ROTATIONS[self.dir][self.next_rotation]
            self.next_rotation = (self.next_rotation + 1) % 3


def parse_input():
    for y, line in enumerate(inputs):
        for x, value in enumerate(line):
            if value in "/\\-|+":
                rails[(y, x)] = value
            elif value in "v^><":
                carts[(y, x)] = Cart(y, x, value)
                if value in "v^":
                    rails[(y, x)] = "|"
                else:
                    rails[(y, x)] = "-"


def part1():
    while True:
        to_sim = sorted(carts.items(), key=lambda c: c[1].y * len(inputs[0]) + c[1].x)
        for pos, cart in to_sim:
            del carts[pos]
            cart.move()
            carts[(cart.y, cart.x)] = cart
            if cart.dir == "X":
                return f"{cart.x},{cart.y}"


def part2():
    tick = 0
    while True:
        tick += 1
        to_sim = sorted(carts.items(), key=lambda c: c[1].y * len(inputs[0]) + c[1].x)
        for pos, cart in to_sim:
            if pos in carts and carts[pos] == cart:
                del carts[pos]
                cart.move()
                if cart.dir != "X":
                    carts[(cart.y, cart.x)] = cart
        if len(carts) == 1:
            cart = list(carts.values())[0]
            return f"{cart.x},{cart.y}"


if __name__ == "__main__":
    parse_input()
    print("Part 1 =", part1())
    carts.clear()
    rails.clear()
    parse_input()
    print("Part 2 =", part2())
